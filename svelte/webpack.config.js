const ProgressBarPlugin = require('progress-bar-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const sveltePreprocessPostcss = require('svelte-preprocess-postcss');

const { HOST = 'localhost', PORT = 8081 } = process.env;

module.exports = (env, argv) => {
  const isProd = argv.mode === 'production';
  return {
    entry: './src/index.js',
    resolve: {
      extensions: ['.mjs', '.js', '.svelte']
    },
    output: {
      path: `${__dirname}/dist`,
      filename: '[name].[hash:8].js',
      chunkFilename: '[name].[hash:8].chunk.js',
      publicPath: '/'
    },
    module: {
      rules: [
        {
          test: /\.svelte$/,
          exclude: /node_modules/,
          use: {
            loader: 'svelte-loader',
            options: {
              emitCss: true,
              hotReload: true,
              preprocess: {
                style: sveltePreprocessPostcss({
                  configFilePath: '',
                  useConfigFile: true
                })
              }
            }
          }
        },
        {
          test: /\.css$/,
          use: [isProd ? MiniCssExtractPlugin.loader : 'style-loader', 'css-loader']
        }
      ]
    },
    mode: argv.mode,
    devServer: {
      host: HOST,
      port: PORT,
      open: true,
      historyApiFallback: true
    },
    plugins: [
      new MiniCssExtractPlugin({
        filename: '[name].[hash:8].css'
      }),
      new ProgressBarPlugin({
        clear: false
      }),
      new HtmlWebpackPlugin({
        cache: true,
        template: './src/index.html',
        favicon: './src/favicon.png',
        filename: './index.html'
      })
    ],
    devtool: isProd ? false : 'source-map',
    stats: 'minimal'
  };
};
