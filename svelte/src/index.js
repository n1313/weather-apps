import App from './components/App';
import 'whatwg-fetch';

const app = new App({
  target: document.getElementById('root')
});
