import React from 'react';
import PropTypes from 'prop-types';

import { getTimestamps, sortObjByField } from '../../utils/utils';

import Navigation from '../../components/Navigation';
import Stations from '../../components/Stations';

import css from './DayPage.css';

class DayPage extends React.PureComponent {
  state = {
    error: false,
    loading: false,
    hasData: false
  };

  componentDidMount() {
    this.loadData();
  }

  loadData = () => {
    const {
      match: {
        params: { date }
      }
    } = this.props;

    this.setState({
      loading: true
    });

    const url = `https://api.data.gov.sg/v1/environment/air-temperature?date=${date}`;

    fetch(url)
      .then((response) => response.json())
      .then(this.parseData)
      .catch(this.requestError);
  };

  requestError = () => {
    this.setState({
      loading: false,
      error: true
    });
  };

  parseData = (response) => {
    if (!response.items.length) {
      this.setState({
        loading: false,
        hasData: true,
        stations: []
      });
      return;
    }

    const { sortedStations, min, max } = this.getSortedStations(response);

    this.setState({
      loading: false,
      hasData: true,
      stations: sortedStations,
      min,
      max
    });
  };

  getSortedStations = (response) => {
    const stations = {};

    const firstTimestamp = response.items[0].timestamp;
    const emptyTimestamps = getTimestamps(
      firstTimestamp.substr(0, 11),
      firstTimestamp.substr(16, 9)
    );

    response.metadata.stations.forEach((s) => {
      stations[s.id] = {
        ...s,
        readings: {
          ...emptyTimestamps
        },
        last: ''
      };
    });

    let min = Infinity;
    let max = -Infinity;

    response.items.forEach((item) => {
      const { timestamp } = item;

      item.readings.forEach((reading) => {
        const station = stations[reading.station_id];
        station.readings[timestamp] = reading.value;
        station.last = timestamp;
        max = Math.max(max, reading.value);
        min = Math.min(min, reading.value);
      });
    });

    const sortedStations = sortObjByField(stations, 'name');
    return {
      min,
      max,
      sortedStations
    };
  };

  renderData = () => {
    const { stations, min, max } = this.state;

    if (!stations.length) {
      return <div className={css.noData}>No data from API</div>;
    }

    return (
      <div className={css.data}>
        <Stations stations={stations} min={min} max={max} />
        <div className={css.footer}>
          <div className={css.stats}>
            {stations.length} stations
            <br />
            Min: {min}°, max: {max}°
          </div>
          <a
            className={css.sourceLink}
            target="_blank"
            rel="noopener noreferrer"
            href="https://data.gov.sg/dataset/realtime-weather-readings"
          >
            data.gov.sg
          </a>
        </div>
      </div>
    );
  };

  renderLoading = () => <div className={css.loading}>Loading...</div>;

  renderError = () => <div className={css.error}>Error!</div>;

  render() {
    const { hasData, loading, error } = this.state;

    const {
      match: {
        params: { date }
      }
    } = this.props;

    return (
      <div className={css.root}>
        <Navigation date={date} />
        {hasData ? this.renderData() : false}
        {loading ? this.renderLoading() : false}
        {error ? this.renderError() : false}
      </div>
    );
  }
}

DayPage.propTypes = {
  match: PropTypes.shape({
    params: PropTypes.shape({
      date: PropTypes.string.isRequired
    })
  })
};

export default DayPage;
