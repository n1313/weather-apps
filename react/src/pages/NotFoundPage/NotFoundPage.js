import React from 'react';

import css from './NotfoundPage.css';

const NotfoundPage = () => {
  return (
    <div className={css.root}>
      Page not found. Go to <a href="/">main page</a>.
    </div>
  );
};

export default NotfoundPage;
