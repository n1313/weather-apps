import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';

import { dmmmmDateFormat, yyyymmddDateFormat } from '../../utils/utils';

import css from './Navigation.css';

const Navigation = (props) => {
  const { date } = props;

  const currentDate = new Date(date);
  const previousDate = new Date(currentDate.getTime() - 86400000);
  const nextDate = new Date(currentDate.getTime() + 86400000);

  const today = new Date();
  // stupid timezones
  const nextDayIsInFuture = yyyymmddDateFormat(nextDate) > yyyymmddDateFormat(today);

  return (
    <div className={css.root}>
      <Link to={`/${yyyymmddDateFormat(previousDate)}`} className={css.previous}>
        &larr; {dmmmmDateFormat(previousDate)}
      </Link>
      <span className={css.current}>{dmmmmDateFormat(currentDate)}</span>
      {nextDayIsInFuture ? (
        <span className={css.next}>{dmmmmDateFormat(nextDate)}</span>
      ) : (
        <Link to={`/${yyyymmddDateFormat(nextDate)}`} className={css.next}>
          {dmmmmDateFormat(nextDate)} &rarr;
        </Link>
      )}
    </div>
  );
};

Navigation.propTypes = {
  date: PropTypes.string.isRequired
};

export default Navigation;
