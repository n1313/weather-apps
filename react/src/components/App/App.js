import React from 'react';
import { HashRouter as Router, Switch, Redirect, Route } from 'react-router-dom';
import 'whatwg-fetch';

import { yyyymmddDateFormat } from '../../utils/utils';

import DayPage from '../../pages/DayPage';
import NotFoundPage from '../../pages/NotFoundPage';

const App = () => {
  const now = new Date();
  const today = yyyymmddDateFormat(now);

  return (
    <Router>
      <Switch>
        <Redirect exact from="/" to={`/${today}`} />
        <Route
          path="/:date([12]\d{3}-[01]\d{1}-[0123]\d{1})"
          // eslint-disable-next-line react/prop-types
          render={(props) => <DayPage key={props.match.params.date} {...props} />}
        />
        <Route component={NotFoundPage} />
      </Switch>
    </Router>
  );
};

export default App;
