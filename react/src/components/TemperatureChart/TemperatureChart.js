import React from 'react';
import PropTypes from 'prop-types';

import { getScaledY } from '../../utils/utils';

import css from './TemperatureChart.css';

class TemperatureChart extends React.Component {
  calculatePointGroups = (width, height, data, min, max) => {
    const groups = [
      { id: 'extreme', threshold: 37 },
      { id: 'too-high', threshold: 35 },
      { id: 'high', threshold: 33 },
      { id: 'medium-high', threshold: 31 },
      { id: 'medium', threshold: 29 },
      { id: 'medium-low', threshold: 27 },
      { id: 'low', threshold: 25 },
      { id: 'very-low', threshold: 21 },
      { id: 'zero', threshold: 0 }
    ];

    const pointGroups = [];

    let lastGroupId;
    let lastPoint;

    Object.keys(data).forEach((timestamp, i) => {
      const reading = data[timestamp];

      if (!reading) {
        lastGroupId = undefined;
        lastPoint = undefined;
        return;
      }

      const currentGroup = groups.find((g) => g.threshold < reading);
      if (lastGroupId !== currentGroup.id) {
        pointGroups.push({
          id: currentGroup.id,
          points: [lastPoint]
        });
        lastGroupId = currentGroup.id;
      }

      const targetGroup = pointGroups[pointGroups.length - 1];

      const x = i;
      const y = getScaledY(reading, height, min, max);
      const point = `${x},${y.toFixed(2)}`;
      lastPoint = point;

      targetGroup.points.push(point);
    });

    return pointGroups;
  };

  renderHourLines = (width, height) => {
    const hours = [0, 6, 12, 18, 24];

    return hours.map((hour) => {
      const x = (width * hour) / 24;
      return <line key={hour} x1={x} y1="0" x2={x} y2={height} />;
    });
  };

  renderChart = (pointGroups) => {
    return pointGroups.map((group, i) => {
      const points = group.points.join(' ');
      const cn = `${css.points} ${css[`points-${group.id}`]}`;
      return <polyline points={points} className={cn} key={i} />;
    });
  };

  render() {
    const { data, min, max, height } = this.props;

    const width = 60 * 24;
    const mid = height / 2;
    const pointGroups = this.calculatePointGroups(width, height, data, min, max);

    return (
      <div className={css.root}>
        <svg
          viewBox={`0 0 ${width} ${height}`}
          width="100%"
          height="100%"
          preserveAspectRatio="none"
          className={css.viewBox}
        >
          <g className={css.mid}>
            <line x1={0} y1={mid} x2={width} y2={mid} />
          </g>
          <g className={css.hours}>{this.renderHourLines(width, height)}</g>
          <g className={css.chart}>{this.renderChart(pointGroups)}</g>
        </svg>
      </div>
    );
  }
}

TemperatureChart.propTypes = {
  data: PropTypes.objectOf(PropTypes.number),
  min: PropTypes.number,
  max: PropTypes.number,
  height: PropTypes.number
};

export default TemperatureChart;
