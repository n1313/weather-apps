import React from 'react';
import PropTypes from 'prop-types';
import throttle from 'p-throttle';

import { getScaledY } from '../../utils/utils';

import TemperatureChart from '../TemperatureChart';

import css from './Stations.css';

class Stations extends React.Component {
  state = {
    showTooltip: false,
    tooltipX: 0
  };

  getTooltipX = throttle(
    (e) => {
      const rect = this.chartsEl.getBoundingClientRect();
      const width = this.chartsEl.clientWidth;
      const x = e.clientX - rect.left;
      this.setState({
        tooltipX: x / width
      });
    },
    1,
    20
  );

  onMouseOver = () => {
    this.setState({
      showTooltip: true
    });
  };

  onMouseOut = () => {
    this.setState({
      showTooltip: false
    });
  };

  onMouseMove = (e) => {
    e.persist();
    this.getTooltipX(e);
  };

  getTimestampForX = (x) => {
    const { stations } = this.props;
    const timestamps = Object.keys(stations[0].readings);

    const keyIndex = Math.round(timestamps.length * x);
    const timestamp = timestamps[keyIndex];

    return timestamp;
  };

  renderTooltip = () => {
    const { stations, height, min, max } = this.props;
    const { tooltipX } = this.state;

    const timestamp = this.getTimestampForX(tooltipX);

    if (!timestamp) {
      return false;
    }

    const readings = stations.map((station) => {
      const value = station.readings[timestamp];
      return {
        id: station.id,
        value
      };
    });

    const style = {
      left: `${tooltipX * 100}%`
    };

    return (
      <div className={css.readingLine} style={style}>
        {readings.map((reading, i) => {
          if (!reading.value) {
            return false;
          }
          const baseHeight = height * i;
          const readingStyle = {
            top: baseHeight + getScaledY(reading.value, height, min, max)
          };
          return (
            <div key={reading.id} className={css.reading} style={readingStyle}>
              {reading.value.toFixed(1)}°
            </div>
          );
        })}
        <div className={css.readingTime}>{timestamp.substr(11, 5)}</div>
      </div>
    );
  };

  render() {
    const { stations, min, max, height } = this.props;
    const { showTooltip } = this.state;

    return (
      <div className={css.root}>
        <div className={css.names}>
          {stations.map((station) => {
            return (
              <div key={station.id} className={css.name}>
                {station.name}
              </div>
            );
          })}
        </div>
        <div
          className={css.charts}
          onMouseOver={this.onMouseOver}
          onFocus={this.onMouseOver}
          onMouseOut={this.onMouseOut}
          onBlur={this.onMouseOut}
          onMouseMove={this.onMouseMove}
          onClick={this.onMouseMove}
          ref={(x) => {
            this.chartsEl = x;
          }}
          role="presentation"
        >
          {showTooltip ? this.renderTooltip() : false}
          {stations.map((station) => {
            return (
              <div className={css.chart} key={station.id}>
                <TemperatureChart data={station.readings} min={min} max={max} height={height} />
              </div>
            );
          })}
          <div className={css.hours}>
            <span className={css.h0}>00:00</span>
            <span className={css.h6}>06:00</span>
            <span className={css.h12}>12:00</span>
            <span className={css.h18}>18:00</span>
            <span className={css.h24}>00:00</span>
          </div>
        </div>
      </div>
    );
  }
}

Stations.defaultProps = {
  height: 40
};

Stations.propTypes = {
  stations: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.string.isRequired,
      name: PropTypes.string.isRequired,
      readings: PropTypes.objectOf(PropTypes.number)
    })
  ),
  min: PropTypes.number,
  max: PropTypes.number,
  height: PropTypes.number
};

export default Stations;
