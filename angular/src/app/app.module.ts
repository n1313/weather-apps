import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { RouterModule, Routes } from '@angular/router';

import { AppRoutingModule } from './app-routing.module';

import { AppComponent } from './components/app/app.component';
import { StationsComponent } from './components/stations/stations.component';
import { TemperatureChartComponent } from './components/temperature-chart/temperature-chart.component';
import { NavigationComponent } from './components/navigation/navigation.component';

import { NotFoundPageComponent } from './pages/not-found-page/not-found-page.component';
import { DayPageComponent } from './pages/day-page/day-page.component';
import { TodayPageComponent } from './pages/today-page/today-page.component';

const appRoutes = [
  { path: '', component: TodayPageComponent },
  { path: ':date', component: DayPageComponent },
  { path: '**', component: NotFoundPageComponent },
];

@NgModule({
  declarations: [
    AppComponent,
    TemperatureChartComponent,
    StationsComponent,
    NavigationComponent,
    NotFoundPageComponent,
    DayPageComponent,
    TodayPageComponent,
  ],
  imports: [RouterModule.forRoot(appRoutes), BrowserModule, AppRoutingModule, HttpClientModule],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
