import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class WeatherDataService {
  constructor(private http: HttpClient) {}

  private url = 'https://api.data.gov.sg/v1/environment/air-temperature';

  loadData(date) {
    const url = `${this.url}?date=${date}`;
    return this.http.get(url);
  }
}
