export function getTimestamps(prefix, suffix) {
  const timestamps = {};

  for (let h = 0; h < 24; h++) {
    for (let m = 0; m < 60; m++) {
      const id = `${prefix}${padNumber(h)}:${padNumber(m)}${suffix}`;
      timestamps[id] = undefined;
    }
  }

  return timestamps;
}

export function padNumber(number) {
  return number > 9 ? String(number) : `0${number}`;
}

export function sortObjByField(obj, field, inverse) {
  return Object.values(obj).sort((a, b) => {
    return inverse ? b[field].localeCompare(a[field]) : a[field].localeCompare(b[field]);
  });
}

export function getScaledY(x, height, min, max, padding = 1) {
  const paddedMin = min - padding;
  const paddedMax = max + padding;
  const range = paddedMax - paddedMin;
  const scale = height / (range + 2);
  return paddedMax - scale * (x - paddedMin);
}

const monthNames = [
  'January',
  'February',
  'March',
  'April',
  'May',
  'June',
  'July',
  'August',
  'September',
  'October',
  'November',
  'December',
];
export function dmmmmDateFormat(date) {
  const d = date.getDate();
  const mmmm = monthNames[date.getMonth()];
  return `${d} ${mmmm}`;
}

export function yyyymmddDateFormat(date) {
  const yyyy = date.getFullYear();
  const mm = padNumber(date.getMonth() + 1);
  const dd = padNumber(date.getDate());
  return `${yyyy}-${mm}-${dd}`;
}
