import { Component, Input, OnInit } from '@angular/core';

import { getScaledY } from '../../utils/utils';

@Component({
  selector: 'app-temperature-chart',
  templateUrl: './temperature-chart.component.html',
  styleUrls: ['./temperature-chart.component.css'],
})
export class TemperatureChartComponent implements OnInit {
  @Input() data;
  @Input() min;
  @Input() max;
  @Input() height;

  pointGroups;
  width = 60 * 24;
  mid;
  hours;

  calculatePointGroups = (width, height, data, min, max) => {
    const groups = [
      { id: 'extreme', threshold: 37 },
      { id: 'too-high', threshold: 35 },
      { id: 'high', threshold: 33 },
      { id: 'medium-high', threshold: 31 },
      { id: 'medium', threshold: 29 },
      { id: 'medium-low', threshold: 27 },
      { id: 'low', threshold: 25 },
      { id: 'very-low', threshold: 21 },
      { id: 'zero', threshold: 0 },
    ];

    const pointGroups = [];

    let lastGroupId;
    let lastPoint;

    Object.keys(data).forEach((timestamp, i) => {
      const reading = data[timestamp];

      if (!reading) {
        lastGroupId = undefined;
        lastPoint = undefined;
        return;
      }

      const currentGroup = groups.find((g) => g.threshold < reading);
      if (lastGroupId !== currentGroup.id) {
        pointGroups.push({
          id: currentGroup.id,
          points: [lastPoint],
        });
        lastGroupId = currentGroup.id;
      }

      const targetGroup = pointGroups[pointGroups.length - 1];

      const x = i;
      const y = getScaledY(reading, height, min, max);
      const point = `${x},${y.toFixed(2)}`;
      lastPoint = point;

      targetGroup.points.push(point);
    });

    return pointGroups;
  };

  ngOnInit() {
    this.mid = this.height / 2;
    this.pointGroups = this.calculatePointGroups(this.width, this.height, this.data, this.min, this.max);

    this.hours = [0, 6, 12, 18, 24].map((hour) => ({
      hour,
      x: (this.width * hour) / 24,
    }));
  }
}
