import { Component, Input, OnInit, ViewChild } from '@angular/core';
import throttle from 'p-throttle';

import { sortObjByField, getScaledY } from '../../utils/utils';

@Component({
  selector: 'app-stations',
  templateUrl: './stations.component.html',
  styleUrls: ['./stations.component.css'],
})
export class StationsComponent implements OnInit {
  @ViewChild('chartsEl') chartsEl;

  @Input() stations;
  @Input() min;
  @Input() max;
  @Input() height = 40;
  showTooltip;
  tooltipX;
  readings;
  timestamp;

  onMouseOver = (e) => {
    this.showTooltip = true;
  };

  onMouseOut = (e) => {
    this.showTooltip = false;
  };

  onMouseMove = (e) => {
    this.getTooltipX(e);
  };

  getTimestampForX = (x) => {
    const timestamps = Object.keys(this.stations[0].readings);

    const keyIndex = Math.round(timestamps.length * x);
    const timestamp = timestamps[keyIndex];

    return timestamp;
  };

  getTooltipX = throttle(
    (e: MouseEvent) => {
      const rect = this.chartsEl.nativeElement.getBoundingClientRect();
      const width = this.chartsEl.nativeElement.clientWidth;
      const x = e.clientX - rect.left;
      this.tooltipX = x / width;
      this.timestamp = this.getTimestampForX(this.tooltipX);

      if (this.timestamp) {
        this.readings = this.stations.map((station, i) => {
          const value = station.readings[this.timestamp];
          if (!value) {
            return false;
          }
          const baseHeight = this.height * i;
          const top = baseHeight + getScaledY(value, this.height, this.min, this.max);
          return {
            id: station.id,
            value,
            top,
          };
        });
      }
    },
    1,
    20,
  );

  ngOnInit() {}
}
