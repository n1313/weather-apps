import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-navigation',
  templateUrl: './navigation.component.html',
  styleUrls: ['./navigation.component.css']
})
export class NavigationComponent implements OnInit {
  @Input() date;
  currentDate;
  previousDate;
  nextDate;
  nextDayIsInFuture;

  ngOnInit() {
    this.currentDate = new Date(this.date);
    this.previousDate = new Date(this.currentDate.getTime() - 86400000);
    this.nextDate = new Date(this.currentDate.getTime() + 86400000);

    const today = new Date();
    // stupid timezones
    this.nextDayIsInFuture = yyyymmddDateFormat(this.nextDate) > yyyymmddDateFormat(today);
  }
}
