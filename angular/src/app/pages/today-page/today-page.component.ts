import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { yyyymmddDateFormat } from '../../utils/utils';

@Component({
  selector: 'app-today-page',
  templateUrl: './today-page.component.html',
  styleUrls: ['./today-page.component.css'],
})
export class TodayPageComponent implements OnInit {
  constructor(private router: Router) {}

  ngOnInit() {
    const now = new Date();
    const today = yyyymmddDateFormat(now);
    this.router.navigate([`/${today}`]);
  }
}
