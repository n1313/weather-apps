import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { WeatherDataService } from '../../weather-data.service';

import { getTimestamps, sortObjByField } from '../../utils/utils';

@Component({
  selector: 'app-day-page',
  templateUrl: './day-page.component.html',
  styleUrls: ['./day-page.component.css'],
})
export class DayPageComponent implements OnInit {
  loading: Boolean = false;
  hasData: Boolean = false;
  error: Boolean = false;
  stations;
  min;
  max;
  date;

  constructor(private route: ActivatedRoute, private weatherDataService: WeatherDataService) {}

  ngOnInit() {
    this.date = this.route.snapshot.paramMap.get('date');
    this.loadData();
  }

  loadData = () => {
    this.loading = true;
    this.weatherDataService.loadData(this.date).subscribe(this.parseData, this.requestError);
  };

  requestError = () => {
    this.loading = false;
    this.error = true;
  };

  getSortedStations = (response) => {
    const stations = {};

    const firstTimestamp = response.items[0].timestamp;
    const emptyTimestamps = getTimestamps(firstTimestamp.substr(0, 11), firstTimestamp.substr(16, 9));

    response.metadata.stations.forEach((s) => {
      stations[s.id] = {
        ...s,
        readings: {
          ...emptyTimestamps,
        },
        last: '',
      };
    });

    let min = Infinity;
    let max = -Infinity;

    response.items.forEach((item) => {
      const { timestamp } = item;

      item.readings.forEach((reading) => {
        const station = stations[reading.station_id];
        station.readings[timestamp] = reading.value;
        station.last = timestamp;
        max = Math.max(max, reading.value);
        min = Math.min(min, reading.value);
      });
    });

    const sortedStations = sortObjByField(stations, 'name');
    return {
      min,
      max,
      sortedStations,
    };
  };

  parseData = (response) => {
    if (!response.items.length) {
      this.loading = false;
      this.hasData = true;
      this.stations = [];
      return;
    }

    const { sortedStations, min, max } = this.getSortedStations(response);

    this.loading = false;
    this.hasData = true;
    this.stations = sortedStations;
    this.min = min;
    this.max = max;
  };
}
